/**
  ******************************************************************************
  * @file    GPIO/IOToggle/main.c
  * @author  MCD Application Team
  * @version V1.2.1
  * @date    20-April-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx.h"
#include "core_cm3.h"
#include <stdbool.h>
#include "io_cfg.h"
#include "kernel/faros.h"
#include "platform.h"

#include"xprintf.h"

#include "sys_ctrl.h"

/** @addtogroup STM32L1xx_StdPeriph_Examples
  * @{
  */

/** @addtogroup IOToggle
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
GPIO_InitTypeDef        GPIO_InitStructure;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
uint32_t stack_blinky1[40];
OSThread blinky1;

void main_blinky1() {
	while (1) {
		BSP_ledRedOn();
		BSP_delay(BSP_TICKS_PER_SEC / 10U);
		BSP_ledRedOff();
		BSP_delay(BSP_TICKS_PER_SEC * 3U / 10U);
	}
}

/* Task blue functin */

uint32_t stack_blinky2[40];
OSThread blinky2;

uint16_t volatile count = 0;

void blue_led_controler(void) {
	//	count++;

	__asm volatile (
				"ldr r0, =count		\n"
				"ldr r1, [r0]		\n"
				"add r1, r1, #1	\n"
				"str r1, [r0, #0x00]\n"
				);

	if (count == 20000) {
		BSP_ledBlueOn();
	}
	else if (count == 40000){
		BSP_ledBlueOff();
		count = 0;
	}

}

void main_blinky2() {
	while (1) {

		/*
		BSP_ledBlueOn();
		BSP_delay(BSP_TICKS_PER_SEC / 10U);
		BSP_ledBlueOff();
		BSP_delay(BSP_TICKS_PER_SEC / 10);
	*/

		blue_led_controler();

	}
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
/* background code: sequential with blocking version */
void faros_app() {
	BSP_init();

	OS_init();

	/* fabricate Cortex-M ISR stack frame for blinky1 */
	OSThread_start(&blinky1,
				   &main_blinky1,
				   stack_blinky1, sizeof(stack_blinky1));

	/* fabricate Cortex-M ISR stack frame for blinky2 */
	OSThread_start(&blinky2,
				   &main_blinky2,
				   stack_blinky2, sizeof(stack_blinky2));
	xprintf("end of fucking\n");
	OS_run();
}

/**
  * @brief  Delay Function.
  * @param  nCount:specifies the Delay time length.
  * @retval None
  */
void Delay(__IO uint32_t nCount)
{
	while(nCount--)
	{
	}
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
