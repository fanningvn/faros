/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif


#include "stm32l1xx.h"
#include <stdbool.h>

extern void faros_app();

#ifdef __cplusplus
}
#endif
#endif /* __MAIN_H */
